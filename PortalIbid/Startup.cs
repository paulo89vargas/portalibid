﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PortalIbid.Startup))]
namespace PortalIbid
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
